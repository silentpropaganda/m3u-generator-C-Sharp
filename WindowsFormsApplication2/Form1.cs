﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();           
            textBox2.Text = "M3u generator 0.9 \r\n";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                String folderName = folderBrowserDialog1.SelectedPath;
                textBox1.Text = folderName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            m3uGen();
        }
        private void m3uGen()
        {
            String directory = textBox1.Text;

            var directoriesProcessed = new HashSet<string>();
            var allDirectoriesProcessed = false;

            while (!allDirectoriesProcessed)
            {
                try
                {
                    reindexAndProcessDirectories(directory, directoriesProcessed);
                    allDirectoriesProcessed = true;
                }
                catch (NastyDirException e)
                {
                    // Initializes the variables to pass to the MessageBox.Show method. 

                    string message = "Problémás könyvtárnév: " + e.dirname;
                    string caption = "Könyvtár probléma";
                    MessageBoxButtons buttons = MessageBoxButtons.AbortRetryIgnore;
                    // Displays the MessageBox.

                    var result = MessageBox.Show(this, message, caption, buttons,
                        MessageBoxIcon.Question, MessageBoxDefaultButton.Button1,
                        MessageBoxOptions.RightAlign);
                    switch(result)
                    {
                        case DialogResult.Abort:
                            return;
                        case DialogResult.Ignore:
                            directoriesProcessed.Add(e.dirname);
                            break;
                        case DialogResult.Retry:
                            break;
                    }
                }
            }
            textBox2.Text += "Végeztem főnök...";
        }

        private void reindexAndProcessDirectories(String directory, HashSet<string> directoriesProcessed)
        {
            var dirs = from dir in
                           Directory.EnumerateDirectories(directory, "*", SearchOption.AllDirectories)
                       select dir;
            foreach (var dir in dirs)
            {
                if (directoriesProcessed.Contains(dir)) continue;

                processDirectory(dir);
                directoriesProcessed.Add(dir);
            }
        }
        

        private void processDirectory(string dir)
        {
            var mp3Files = musicGet(dir);
            var resolvedPaths = mp3Files.Select(file => GetRelativePath(file, dir));
            var m3uFilename = generateM3uName(dir);


            System.IO.StreamWriter m3u = new System.IO.StreamWriter(m3uFilename);
            m3u.WriteLine(String.Join("\r\n", resolvedPaths));
            m3u.Close();
        }

        private IEnumerable<string> musicGet(string dir)
        {            
            //var mp3Files = Directory.EnumerateFiles(dir, "*.flac", SearchOption.AllDirectories);
            IEnumerable<string> mp3Files = new List<string>();
            foreach (var type in listBox1.Items) { 
                mp3Files = mp3Files.Concat(Directory.EnumerateFiles(dir, "*." + type, SearchOption.AllDirectories));
            }
            
            return mp3Files;
        }

        class NastyDirException : Exception {
            public string dirname;
            public NastyDirException( string dirname_ ) { dirname = dirname_; }
        }

        private string generateM3uName(string dir)
        {
            DirectoryInfo dirName = new DirectoryInfo(dir);
            var m3uFilename = dir + "\\" + dirName.Name + ".m3u";
            //var filenameChanged = false;
            const int MaxFilenameLength = 247;
            if (m3uFilename.Length > MaxFilenameLength)
            {
                throw new NastyDirException(dir);
            }
            //    m3uFilename = dir + "\\_playlist.m3u";
            //    filenameChanged = true;
            //    if (m3uFilename.Length > MaxFilenameLength)
            //    {
            //        throw new Exception("Rossz filenev");
            //    }
            //    if (filenameChanged)
            //    {
            //        textBox2.Text += "Megváltotattuk: " + dir + "\r\n  playlist nevet " + m3uFilename + "-re\r\n";
            //    }
            //}
            return m3uFilename;
        }

        private string GetRelativePath(string filespec, string folder)
        {
            Uri pathUri = new Uri(filespec);
            // Folders must end in a slash
            if (!folder.EndsWith(Path.DirectorySeparatorChar.ToString()))
            {
                folder += Path.DirectorySeparatorChar;
            }
            Uri folderUri = new Uri(folder);
            return Uri.UnescapeDataString(folderUri.MakeRelativeUri(pathUri).ToString().Replace('/', Path.DirectorySeparatorChar));
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox3.Text != "")
            {
                listBox1.Items.Add(textBox3.Text);
            };
            textBox3.Text = "";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            for (int i = listBox1.SelectedIndices.Count - 1; i >= 0; i--)
            {
                listBox1.Items.RemoveAt(listBox1.SelectedIndices[i]);
            }
        }

    }
}
